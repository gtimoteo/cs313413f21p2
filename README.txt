Gaudwin N Timoteo 9/24/21

ArrayList:
n=10 - 2s
n=100 - 2s
n=1000 - 7s
n=10000 - 9s

TestIterator.java TODO Questions:
// TODO also try with a LinkedList - does it make any difference?
No, the tests will still run completely no matter if we use ArrayList or LinkedList. Both Interfaces are under List.
// TODO what happens if you use list.remove(Integer.valueOf(77))?
The test fails and we get a ConcurrentModificationException

TestList.java TODO Questions:
// TODO also try with a LinkedList - does it make any difference?
Like the answer to above: there are no differences in the tests' success since both are Lists.
// TODO answer: what does this method do?
The method removes an object at an index, in this case: removing 77 from index 5
// TODO answer: what does this one do?
This removes the first instance that an object value of 5 is found. In this case: removing 5 from index 4

TestPerformance.java TODO Questions & Time Performance:
// TODO answer: which of the two lists performs better as the size increases?
Both of the two lists perform better at different operations when size increases.
As size increases, ArrayList performs indexing better and LinkedList performs Add/Remove better.

n = 10:
testArrayListAccess         16ms
testArrayListAddRemove      25ms
testLinkedListAccess        13ms
testLinkedListAddRemove     31ms

n = 100:
testArrayListAccess         15ms
testArrayListAddRemove      45ms
testLinkedListAccess        30ms
testLinkedListAddRemove     34ms

n = 1000:
testArrayListAccess         18ms
testArrayListAddRemove      195ms
testLinkedListAccess        492ms
testLinkedListAddRemove     36ms

n = 10000:
testArrayListAccess         51ms
testArrayListAddRemove      2017ms
testLinkedListAccess        6370ms
testLinkedListAddRemove     84ms

n = 100000:
testArrayListAccess         33ms
testArrayListAddRemove      23975ms
testLinkedListAccess        73526ms
testLinkedListAddRemove     47ms